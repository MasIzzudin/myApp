import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import axios from 'axios';
import { DaftarAlbums } from './DaftarAlbums';


export class AppList extends Component {
    state = { albums: [] };


    componentWillMount() {
        axios.get('https://rallycoding.herokuapp.com/api/music_albums')
         .then(response => this.setState({ albums: response.data }));
    }

    renderAlbums() { 
        return this.state.albums.map(daftar => <DaftarAlbums key={daftar.title} data={daftar} />);
    }

    render() {
        console.log(this.state);

    const { listStyle } = style;

        return (
            <ScrollView style={listStyle}>
                {this.renderAlbums()}
            </ScrollView>
        );
    }
}

const style = {
    listStyle: {
        paddingLeft: 5
    }
};
