import React from 'react';
import { Text, View, Image, Linking } from 'react-native';
import { Card } from './Card';
import { CardSection } from './CardSection';
import { Button } from './Button';

export const DaftarAlbums = ({ data }) => {
const { title, artist, thumbnail_image, image, url } = data;
const { 
    HeaderStyle, 
    ImageStyle, 
    ImageContainerStyle,
    TextStyle,
    pictureStyle

} = style;

    return (
        <Card>
            <CardSection>
                <View style={ImageContainerStyle}>
                    <Image 
                        source={{ uri: thumbnail_image }} 
                        style={ImageStyle}                    
                    />
                </View>

                <View style={HeaderStyle}>
                    <Text style={TextStyle}>{ title }</Text>
                    <Text>{ artist }</Text>
                </View>
            </CardSection>


            <CardSection>
                <Image 
                    source={{ uri: image }}
                    style={pictureStyle}
                />
            </CardSection>


            <CardSection>
                <Button onPincet={() => Linking.openURL(url)} >
                    Order Now!
                </Button>
            </CardSection>
        </Card>
    );
};

const style = {
    HeaderStyle: {
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    ImageStyle: {
        height: 50,
        width: 50
    },
    ImageContainerStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10
    },
    TextStyle: {
        fontSize: 20,
        color: '#ff0000'
    },
    pictureStyle: {
        height: 300,
        flex: 1,
        width: null
    }
};
