import React from 'react';
import { AppRegistry, View } from 'react-native';
import { Header } from './src/components/Header';
import { AppList } from './src/components/AppList';

const myApp = () => (
  <View style={{ flex: 1 }}>
     <Header />
     <AppList />
  </View>
  );

AppRegistry.registerComponent('myApp', () => myApp);
